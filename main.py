def print_hi(name):
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


def feature1(string: str) -> None:
    print(f"Hi, This is feature1\n{string}")


def feature2(string: str) -> None:
    print(f"Hi from feature2 {string}")


def feature3(string: str) -> None:
    print(f"Hi from feature2 {string}")


if __name__ == '__main__':
    print_hi('PyCharm')
